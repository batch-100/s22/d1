const Course = require('../model/Course');

module.exports.add = (params) => {
    let course = new Course({
        name: params.name,
        description: params.description,
        price: params.price
    })

    return course.save().then((course, err) => {
        return (err) ? false : true
    })
}

module.exports.getAll = () => {
    return Course.find({ isActive: true }).then(courses => courses)
}

module.exports.get = (params) => {
    return Course.findById(params.courseId).then(courses => courses)
}

//add controller for updating  a course
module.exports.update = (params) => {
    const updates = {
        name: params.name,
        description: params.description,
        price: params.price
    }

    return Course.findByIdAndUpdate(params.courseId, updates).then((doc, err) => {
        return (err) ? false : true
    })
}
//add a controller for soft deleting or archiving a course
module.exports.archive = (params) => {
    //1. isActive: true of course will change into false

    //2. course schema, we will use findByIdAndUpdate will return a true or false
    const archives = { isActive: false }
    return Course.findByIdAndUpdate(params.courseId, archives).then((softD, err)=>{
    	return (err) ? false : true
    })
}