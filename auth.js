//create an auth.js file, it will contains the logic for authorization via tokens and create function to create access token

const jwt = require('jsonwebtoken');
const secret = 'CourseBookingAPI';

module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}
	//jwt.sign(payload, secretKey, options)
	return jwt.sign(data, secret, {});

}

//add the additional logic to verify and decode the user information
//next passes on the request to the next middleware function/route/request handler in the stack
module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization

	//console.log(token);

	if (typeof token !== 'undefined') {
		//used to get the token only
		//removes the "bearer" from the received token
		token = token.slice(7, token.length)

		return jwt.verify(token, secret, (err, data) => {
			//next() passes the request to the next callback function in the route
			//next callback function = (req, res) => {}
			return (err) ? res.send({ auth: 'failed'}) : next()
		})
	}else {
		return res.send({ auth: 'failed'})
	}
}

module.exports.decode = (token) => {
	if (typeof token !== 'undefined') {
		token = token.slice(7, token.length)

		return jwt.verify(token, secret, (err, data) => {
			//console.log(err)

			//{ complete: true} grabs both the request header and the payload
			return (err) ? null : jwt.decode(token, { complete: true}).payload
		})
	}else {
		return null
	}
}